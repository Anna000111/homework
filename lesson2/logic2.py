def if_3_do(n):
    """
    Функция if_3_do.

    Принимает число.
    Если оно больше 3, то увеличить число на 10, иначе уменьшить на 10.
    Вернуть результат.
    """
    if n > 3:
        return n + 10
    else:
        return n - 10


def main():
    is_continue = ''
    while is_continue != 'yes':
        num = int(input('Введите число: '))
        print(if_3_do(num))
        is_continue = input("Хотите закончить (yes)? ")

    print("Спасибо за участие!")


if __name__ == '__main__':
    main()
