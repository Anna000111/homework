def print_hi(n):
    """
    Функция print_hi.

    Принимает число n.
    Выведите на экран n раз фразу "Hi, friend!"
    """
    print('Hi, friend!' * n)


if __name__ == '__main__':
    print_hi(5)
    in_str = input('Введите число (от 1 до 100). ')
    number = int(in_str)
    print_hi(number)
