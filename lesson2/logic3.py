def check_sum(n1, n2, n3):
    """
    Функция check_sum.

    Принимает 3 числа.
    Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
    """
    if n1 == n2 + n1 or n2 == n3 + n1 or n1 == n3 + n2:
        return True
    else:
        return False


def main():
    is_continue = ''
    while is_continue != 'yes':
        print('Введите три числа')
        num1 = int(input("Введите 1-е число: "))
        num2 = int(input("Введите 2-е число: "))
        num3 = int(input("Введите 3-е число: "))
        print(check_sum(num1, num2, num3))
        is_continue = input("Хотите закончить (yes)? ")

    print("Спасибо за участие!")


if __name__ == '__main__':
    main()
