def count_odd_num(number):
    """
    Функция count_odd_num.

    Принимает натуральное число.
    Верните количество нечетных цифр в этом числе.
    """
    if number > 0:
        count = 0
        for char in str(number):
            if int(char) % 2 == 1:
                count = count + 1
        return count
    else:
        print("Число должно быть натуральным (> 0)")


if __name__ == '__main__':
    while True:
        num = int(input('Введите натуральное число (для завершения 0): '))
        if num == 0:
            break
        else:
            print(count_odd_num(num))

    print('Спасибо за участие!')
