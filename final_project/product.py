from datetime import date


class Product:
    count = 0

    def __init__(self, name, weight, life):
        self.name = name
        self.weight = weight
        self.life = life

        self.id = 1000 + Product.count
        Product.count += 1

    def __str__(self):
        return f'id: {self.id} | Имя: {self.name} | Масса: {self.weight} | Срок годности: {self.life}'
