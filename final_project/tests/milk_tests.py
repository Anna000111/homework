import pytest

from final_project.product import Product
from final_project.milk import Milk


class TestMilk:

    def setup_method(self):
        Milk.count = 0

    def test_init_ok(self):
        milk = Milk('Молоко1', '1 литр', '1 год')
        assert milk.name == 'Молоко1'
        assert milk.weight == '1 литр'
        assert milk.life == '1 год'

        assert isinstance(milk, Product)

        assert Milk.count == 1

    def test_milk_count_ok(self):
        assert Milk.count == 0

        num = 10
        for i in range(num):
            Milk('Молоко1', '1 литр', '1 год')

        assert Milk.count == num

    @pytest.mark.parametrize("name, weight, life",
                             [
                                 ('Milk1', '1 liter', '1 year'),
                                 ('Milk2', '1 liter', '6 months'),
                             ])
    def test_str_ok(self, name, weight, life):
        milk = Milk(name, weight, life)
        assert str(milk) == f'Milk: Имя: {name} | Масса: {weight} | Срок годности: {life}'
