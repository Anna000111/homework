from final_project.product import Product


class TestProduct:
    my_product_feats = None

    @classmethod
    def setup_class(cls):
        cls.my_product_feats = ('Молоко1', '1 литр', '1 год')

    def setup_method(self):
        Product.count = 0

    def test_init_ok(self):
        my_product = Product(*self.__class__.my_product_feats)
        assert my_product.name == 'Молоко1'
        assert my_product.weight == '1 литр'
        assert my_product.life == '1 год'
        assert my_product.count == 1

    def test_str_ok(self):
        my_product = Product(*self.__class__.my_product_feats)
        assert str(my_product) == 'id: 1000 | Имя: Молоко1 | Масса: 1 литр | Срок годности: 1 год'

    def test_count_ok(self):
        num = 20
        for i in range(num):
            Product(*self.__class__.my_product_feats)

        assert Product.count == num
