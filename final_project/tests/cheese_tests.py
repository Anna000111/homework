from final_project.product import Product
from final_project.cheese import Cheese


class TestCheese:
    my_product_feats = None

    @classmethod
    def setup_class(cls):
        cls.my_product_feats = ('Сыр1', '500 грамм', '1 год', 'Моцарелла', 'белый')

    def setup_method(self):
        Product.count = 0

    def test_init_ok(self):
        cheese = Cheese(*self.__class__.my_product_feats)
        assert cheese.name == 'Сыр1'
        assert cheese.weight == '500 грамм'
        assert cheese.life == '1 год'
        assert cheese.kind == 'Моцарелла'
        assert cheese.color == 'белый'

        assert isinstance(cheese, Product)

        assert Product.count == 1

    def test_cheese_count(self):
        assert Cheese.count == 0

        num = 20
        for i in range(num):
            Cheese('Сыр1', '500 грамм', '1 год', 'Моцарелла', 'белый')

        assert Cheese.count == num
