from final_project.product import Product
from final_project.milk import Milk
from final_project.cheese import Cheese
from final_project.warehouse import Warehouse


class TestWarehouse:
    @classmethod
    def setup_class(cls):
        print("setup_class called once for the class")

    @classmethod
    def teardown_class(cls):
        print("teardown_class called once for the class")

    def setup_method(self):
        print("setup_method called for every method")

    def teardown_method(self):
        print("teardown_method called for every method")

    def test_init_with_file_ok(self):
        my_new_warehouse = Warehouse('Мой тестовый склад', products_backup_filename='products_for_tests.pkl')
        assert my_new_warehouse.title == 'Мой тестовый склад'
        assert len(my_new_warehouse.products) == 2

    def test_init_without_file_ok(self):
        my_new_warehouse = Warehouse('Мой тестовый склад', products_backup_filename='not_exist.pkl')
        assert my_new_warehouse.title == 'Мой тестовый склад'
        assert my_new_warehouse.products == {}

    def test_get_title_ok(self):
        my_new_warehouse = Warehouse('Мой тестовый склад', products_backup_filename='products_for_tests.pkl')
        assert my_new_warehouse.get_title() == '*** Мой тестовый склад ***'

    def test_get_products_by_category_ok(self):
        my_new_warehouse = Warehouse('Мой тестовый склад', products_backup_filename='products_for_tests.pkl')
        products_by_meat = my_new_warehouse.get_products_by_category('Молочная продукция')
        assert len(products_by_meat) == 3

    def test_get_products_by_category_err(self):
        my_new_warehouse = Warehouse('Мой тестовый склад', products_backup_filename='products_for_tests.pkl')
        products_by_meat = my_new_warehouse.get_products_by_category('Хлеб')
        assert products_by_meat is None

    def test_get_product_with_min_count(self):
        Product.count = 0
        Milk.count = 0
        Cheese.count = 0

        my_warehouse = Warehouse(title='Просто склад', should_load_backup=False)

        assert my_warehouse.get_product_with_min_count() == []

        milk_1 = Milk(name='Молоко1', weight='1 литр', life='1 год')
        milk_2 = Milk('Молоко2', '1 литр', '6 месяцев')
        my_warehouse.add_product('Молочная продукция', milk_1)
        my_warehouse.add_product('Молочная продукция', milk_2)
        cheese_1 = Cheese('Сыр1', '500 грамм', '1 год', 'Моцарелла', 'белый')
        cheese_2 = Cheese('Сыр2', '500 грамм', '2 года', 'Чеддер', 'жёлтый')
        my_warehouse.add_product('Сыры', cheese_1)
        my_warehouse.add_product('Сыры', cheese_2)
        assert my_warehouse.get_product_with_min_count() == ['Milk', 'Cheese']

        cheese_3 = Cheese('Сыр3', '500 грамм', '2 года', 'Пармезан', 'жёлтый')
        my_warehouse.add_product('Сыры', cheese_3)
        assert my_warehouse.get_product_with_min_count() == ['Milk']

        milk_3 = Milk('Молоко0', '2 литра', '1 год')
        my_warehouse.add_product('Молочная продукция', milk_3)
        assert my_warehouse.get_product_with_min_count() == ['Milk', 'Cheese']
