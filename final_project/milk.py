from datetime import date

from final_project.product import Product


class Milk(Product):
    count = 0

    def __init__(self, name, weight, life):
        super().__init__(name, weight, life)

        Milk.count += 1


def main():
    milk_1 = Milk('Молоко1', '1 литр', '1 год')
    print(f'milk_count: {Milk.count}')
    milk_2 = Milk('Молоко2', '1 литр', '6 месяцев')
    print(f'milk_count: {Milk.count}')

    print(f'count: {Milk.count}')


if __name__ == '__main__':
    main()
