class NotProductException(Exception):
    """
    Raised when the input value isn't subclass/class Product
    """
    pass
