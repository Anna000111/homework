from datetime import date

from final_project.product import Product


class Cheese(Product):
    count = 0

    def __init__(self, name, weight, life, kind, color):
        super().__init__(name, weight, life)
        self.kind = kind
        self.color = color

        Cheese.count += 1

    @classmethod
    def from_birth_year(cls, name, weight, life, kind, color):
        return cls(name, weight, life, kind, color)

    def say_name(self):
        return f'Название товара {self.name}'

    def kind(self):
        return f'Вид сыра {self.kind}'

    def __str__(self):
        return f'Cheese: {super().__str__()} | Вид сыра: {self.kind} | Цвет сыра {self.color}'
