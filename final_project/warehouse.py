import pickle
import os.path

from final_project.product import Product
from final_project.milk import Milk
from final_project.cheese import Cheese
from final_project.not_product_exception import NotProductException


class Warehouse:
    def __init__(self, title="Склад",
                 owner='Человек',
                 products_backup_filename='products.pkl',
                 should_load_backup=True):
        self.title = title
        self.owner = owner
        self.products = {}

        self.products_backup_filename = products_backup_filename

        if should_load_backup and os.path.exists(self.products_backup_filename) and os.path.isfile(
                self.products_backup_filename):
            self._load_products()

    @property
    def info_str(self):
        """
        Информация о складе
        :return: Строка с названием и владельцем
        """
        return f'{"*" * 100}\nНазвание: {self.title}\nВладелец: {self.owner}\n{"*" * 100}'

    def __str__(self):
        return f'Название: {self.title}\nВладелец: {self.owner}'

    def _load_products(self):
        with open(self.products_backup_filename, 'rb') as my_file:
            self.products = pickle.load(my_file)

            product_count = 0
            milk_count = 0
            cheese_count = 0
            for products in self.products.values():
                for product in products:
                    if isinstance(product, Product):
                        product_count += 1
                        Product.count = product_count

                    if isinstance(product, Milk):
                        milk_count += 1
                        Milk.count = milk_count

                    if isinstance(product, Cheese):
                        cheese_count += 1
                        Cheese.count = cheese_count

    def _save_products(self):
        with open(self.products_backup_filename, 'wb') as my_file:
            pickle.dump(self.products, my_file)

    def add_product(self, category, product):
        if not isinstance(product, Product):
            raise NotProductException(f"Object {product} (type = {type(product)} isn't Product instance")

        if category not in self.products.keys():
            self.products[category] = []

        self.products[category].append(product)

        self._save_products()

    def add_products(self, category, *products):
        if category not in self.products.keys():
            self.products[category] = []

        for product in products:
            if not isinstance(product, Product):
                raise NotProductException(f"Object {product} (type = {type(product)} isn't Product instance")

            self.products[category].append(product)

        self._save_products()

    def get_all_products(self):
        result_string = ''
        for category, products in self.products.items():
            result_string += category + ':\n'

            for product in products:
                result_string += f'\t{str(product)}\n'

            result_string += '\n'

        return result_string

    def get_title(self):
        return f'*** {self.title} ***'

    def get_products_by_category(self, category):
        return self.products.get(category)

    def get_product_with_min_count(self):
        if len(self.products) == 0:
            return []

        product_types = {}
        for products in self.products.values():
            for product in products:
                product_types[product.__class__.__name__] = product.count

        final_elements = []

        min_n = min(product_types.values())
        for t, n in product_types.items():
            if n <= min_n:
                final_elements.append(t)

        return final_elements

    def del_product_by_feat(self, product_type, **product_feats):
        is_found = False
        category_found = ''
        product_index_found = None
        for category, products in self.products.items():
            for idx, product in enumerate(products):
                if product == product_feats:
                    category_found = category
                    product_index_found = idx
                    is_found = True
                    break

            if is_found:
                break

        if is_found:
            del self.products[category_found][product_index_found]
            return True
        return False


def demos():
    my_warehouse = Warehouse(title='Просто склад')
    print(my_warehouse.get_title())

    my_warehouse.title = 'Обычный склад'
    print(my_warehouse.get_title())

    print('-' * 50)

    milk_1 = Milk(name='Молоко1', weight='1 литр', life='1 год')
    milk_2 = Milk('Молоко2', '1 литр', '6 месяцев')
    my_warehouse.add_product('Молочная продукция', milk_1)
    my_warehouse.add_product('Молочная продукция', milk_2)

    cheese_1 = Cheese('Сыр1', '500 грамм', '1 год', 'Моцарелла', 'белый')
    cheese_2 = Cheese('Сыр2', '500 грамм', '2 года', 'Чеддер', 'жёлтый')
    cheese_3 = Cheese('Сыр3', '500 грамм', '2 года', 'Пармезан', 'жёлтый')
    my_warehouse.add_product('Сыры', cheese_1)
    my_warehouse.add_product('Сыры', cheese_2)
    my_warehouse.add_product('Сыры', cheese_3)

    my_warehouse.get_all_products()

    print('-' * 50)

    min_count = my_warehouse.get_product_with_min_count()
    print(f'min count: {min_count}')

    print('-' * 50)

    defaut_warehouse = Warehouse()
    print(defaut_warehouse.get_title())

    defaut_warehouse.add_products('Молочная продукция', milk_1, milk_2)

    defaut_warehouse.add_products('Сыры', cheese_1, cheese_2, cheese_3)
    defaut_warehouse.get_all_products()


def demos_with_exceptions():
    my_new_warehouse = Warehouse('Мой новый склад', products_backup_filename='products_new.pkl')
    milk_0 = Milk('Молоко0', '2 литра', '1 год')
    milk_1 = Milk(name='Молоко1', weight='1 литр', life='1 год')
    milk_2 = Milk('Молоко2', '1 литр', '6 месяцев')
    my_new_warehouse.add_products('Молочная продукция', milk_0, milk_1, milk_2)

    cheese_1 = Cheese('Сыр1', '500 грамм', '1 год', 'Моцарелла', 'белый')
    cheese_2 = Cheese('Сыр2', '500 грамм', '2 года', 'Чеддер', 'жёлтый')
    cheese_3 = Cheese('Сыр3', '500 грамм', '2 года', 'Пармезан', 'жёлтый')
    my_new_warehouse.add_products('Сыры', cheese_1, cheese_2, cheese_3)

    my_new_warehouse.get_all_products()

    print('=' * 100)

    try:
        my_new_warehouse.add_product('Set', {1, 3, 4})
    except NotProductException as exp:
        print(f'Получена NotProductException ошибка: {exp}')
    except Exception as exp:
        print(f'Получена неизвестная ошибка: {exp}')
    else:
        print('Обновленный список:')
        my_new_warehouse.get_all_products()
    finally:
        print('Спасибо!')

    try:
        my_new_warehouse.add_product([1, 2, 3], cheese_1)
    except NotProductException as exp:
        print(f'Получена NotProductException ошибка: {exp}')
    except Exception as exp:
        print(f'Получена неизвестная ошибка: {exp}')
    else:
        print('Обновленный список:')
        my_new_warehouse.get_all_products()
    finally:
        print('Спасибо!')

    try:
        my_new_warehouse.add_product('Сыры', cheese_1)
    except NotProductException as exp:
        print(f'Получена NotProductException ошибка: {exp}')
    except Exception as exp:
        print(f'Получена неизвестная ошибка: {exp}')
    else:
        print('Обновленный список:')
        my_new_warehouse.get_all_products()
    finally:
        print('Спасибо!')


def main():
    try:
        my_new_warehouse = Warehouse('Мой новый склад', products_backup_filename='products_new.pkl')
        milk_0 = Milk('Молоко0', '2 литра', '1 год')
        milk_1 = Milk(name='Молоко1', weight='1 литр', life='1 год')
        milk_2 = Milk('Молоко2', '1 литр', '6 месяцев')
        my_new_warehouse.add_products('Молочная продукция', milk_0, milk_1, milk_2)

        cheese_1 = Cheese('Сыр1', '500 грамм', '1 год', 'Моцарелла', 'белый')
        cheese_2 = Cheese('Сыр2', '500 грамм', '2 года', 'Чеддер', 'жёлтый')
        cheese_3 = Cheese('Сыр3', '500 грамм', '2 года', 'Пармезан', 'жёлтый')
        my_new_warehouse.add_products('Сыры', cheese_1, cheese_2, cheese_3)

        print(my_new_warehouse.info_str)

    except NotProductException as exp:
        print(f'Получена NotProductException ошибка: {exp}')
    except Exception as exp:
        print(f'Получена неизвестная ошибка {exp}')
    else:
        print('Обновленный список')
        print(my_new_warehouse.get_all_products())
    finally:
        print('Спасибо!')


if __name__ == '__main__':
    main()
