def count_word(my_list, word):
    """
    Функция count_word.

    Принимает 2 аргумента:
        список слов my_list и
        строку word.

    Возвращает количество word в списке my_list.
    """
    return my_list.count(word)


def main():
    is_continue = ''
    while is_continue != 'yes':
        list_str = input("Введите слова через пробел: ")
        word_str = input("Введите слово: ")
        list_str_spl = list_str.split(' ')
        print(count_word(list_str_spl, word_str))
        is_continue = input("Хотите закончить (yes)? ")

    print("Спасибо за участие!")


if __name__ == '__main__':
    main()
