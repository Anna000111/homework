def print_info_for_3(my_set_left, my_set_mid, my_set_right):
    """
    Функция print_info_for_3.

    Принимает 3 аргумента: множества my_set_left, my_set_mid и my_set_right.

    Выводит информацию о: равенстве множеств, имеют ли они общие элементы, являются ли они подмножествами друг друга.
    """
    print(f'my_set_left: {my_set_left}')
    print(f'my_set_mid: {my_set_mid}')
    print(f'my_set_right: {my_set_right}')

    print(f'Равенство: {my_set_left == my_set_right == my_set_mid}')
    print(f'Общие элементы: {my_set_left.intersection(my_set_right), my_set_left.intersection(my_set_mid)}')
    print(f'my_set_left подмножество my_set_right: {my_set_left.issubset(my_set_right)}')
    print(f'my_set_right подмножество my_set_left: {my_set_right.issubset(my_set_left)}')
    print(f'my_set_right подмножество my_set_mid: {my_set_mid.issubset(my_set_left)}')
    print(f'my_set_right подмножество my_set_mid: {my_set_mid.issubset(my_set_right)}')


def main():
    is_continue = ''
    while is_continue != 'yes':
        set_str_1 = input("Введите значения через пробел: ")
        set_str_2 = input("Введите значения через пробел: ")
        set_str_3 = input("Введите значения через пробел: ")
        set_1 = set(set_str_1.split()) if len(set_str_1) > 0 else set()
        set_2 = set(set_str_2.split()) if len(set_str_2) > 0 else set()
        set_3 = set(set_str_3.split()) if len(set_str_3) > 0 else set()
        print_info_for_3(set_1, set_2, set_3)
        is_continue = input("Хотите закончить (yes)? ")

    print("Спасибо за участие!")


if __name__ == '__main__':
    main()
