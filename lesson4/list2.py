def magic_mul(my_list):
    """
    Функция magic_mul.

    Принимает 1 аргумент: список my_list.

    Возвращает список, который состоит из
        [первого элемента my_list]
        + [три раза повторенных списков my_list]
        + [последнего элемента my_list].

    Пример: входной список [1,  ‘aa’, 99], результат [1, 1,  ‘aa’, 99, 1,  ‘aa’, 99, 1,  ‘aa’, 99, 99].
    (* Если индекс выходит за пределы списка, то последний.
    Если список пустой - вернуть пустой список. Нумерация элементов начинается с 0.)
    """
    print(f'Список для обработки: {my_list}')

    if len(my_list) == 0:
        return []

    result_list = [my_list[:1], my_list*3, my_list[-1]]
    return result_list


def main():
    is_continue = ''
    while is_continue != 'yes':
        list_str = input("Введите список чисел через пробел: ")
        list_str_spl = list_str.split(' ')
        print(magic_mul(list_str_spl))
        is_continue = input("Хотите закончить (yes)? ")

    print("Спасибо за участие!")


if __name__ == '__main__':
    main()
