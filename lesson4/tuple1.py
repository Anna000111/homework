def len_count_first(my_tuple, num):
    """
    Функция len_count_first.

    Принимает 2 аргумента: кортеж my_tuple и число num.

    Возвращает кортеж состоящий из длины кортежа, количества чисел num в кортеже my_tuple и первого элемента кортежа.

    Пример: my_tuple=(‘55’, ‘aa’, 66) num = 66, результат (3, 1, ‘55’).
    """
    print(f"Мой кортеж: {my_tuple}")

    return len(my_tuple), my_tuple.count(num), my_tuple[0] if len(my_tuple) > 0 else None


def main():
    is_continue = ''
    while is_continue != 'yes':
        list_str_1 = input("Введите список значений через пробел: ")
        list_str_2 = input("Введите значение: ")
        list_str_spl_1 = tuple(list_str_1.split(' ')) if len(list_str_1) > 0 else ()
        print(len_count_first(list_str_spl_1, list_str_2))
        is_continue = input("Хотите закончить (yes)? ")

    print("Спасибо за участие!")


if __name__ == '__main__':
    main()
