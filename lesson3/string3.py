def check_substr(str_1, str_2):
    """
    Функция check_substr.

    Принимает две строки.
    Если меньшая по длине строка содержится в большей, то возвращает True, иначе False.
    """
    str_diff = len(str_1) - len(str_2)
    if str_diff > 0:
        return str_2 in str_1
    else:
        return str_1 in str_2


def main():
    is_continue = ''
    while is_continue != 'yes':
        string1 = input("Введите строку 1: ")
        string2 = input("Введите строку 2: ")
        print(check_substr(string1, string2))
        is_continue = input("Хотите закончить (yes)? ")

    print("Спасибо за участие!")


if __name__ == '__main__':
    main()
