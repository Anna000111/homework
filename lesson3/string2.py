def print_nth_symbols(my_str, n):
    """
    Функция print_nth_symbols.

    Принимает строку и натуральное число n.
    Вывести символы с индексом n, n*2, n*3 и так далее.
    """
    print(my_str[n::n])


def main():
    is_continue = ''
    while is_continue != 'yes':
        string = input("Введите строку: ")
        n = int(input("Введите n: "))
        print_nth_symbols(string, n)
        is_continue = input("Хотите закончить (yes)? ")

    print("Спасибо за участие!")


if __name__ == '__main__':
    main()
