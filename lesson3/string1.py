def print_symbols_if(my_str):
    """
    Функция print_symbols_if.

    Принимает строку.
    Вывести первые три символа и последний три символа, если длина строки больше 5.
    Иначе вывести первый символ столько раз, какова длина строки.
    """
    return (my_str[:3] + my_str[-3:]) if len(my_str) > 5 else my_str[0]


def main():
    is_continue = ''
    while is_continue != 'yes':
        string = input("Введите строку: ")
        print(print_symbols_if(string))
        is_continue = input("Хотите закончить (yes)? ")

    print("Спасибо за участие!")


if __name__ == '__main__':
    main()
